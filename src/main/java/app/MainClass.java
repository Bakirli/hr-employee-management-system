package app;

import app.controller.ControllerByList;
import app.entity.Employee;

import java.math.BigDecimal;

public class MainClass {

    public static void main(String[] args) {

        ControllerByList controller = new ControllerByList();

        Employee employee1 = Employee.of("Will", "Smith", "Java Developer", BigDecimal.valueOf(1500));
        Employee employee2 = Employee.of("Will", "Smith", "Java Developer", BigDecimal.valueOf(1200));
        Employee employee3 = Employee.of("Will", "Smith", "Java Developer", BigDecimal.valueOf(2000));
        controller.addEmployee(employee1);
        controller.addEmployee(employee2);
        controller.addEmployee(employee3);
        System.out.println(controller.getEmployees());
        controller.deleteEmployee(employee2);
        System.out.println(controller.getEmployees());

        employee3.setSalary(BigDecimal.valueOf(3000));
        controller.updateEmployee(employee3);

        System.out.println(controller.getEmployeeByID(2));
        System.out.println(controller.getEmployees());
    }
}
