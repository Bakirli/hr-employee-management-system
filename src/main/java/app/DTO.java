package app;

import app.entity.Employee;

import java.util.List;
import java.util.Optional;

public interface DTO {

    boolean addEmployee(Employee employee);
    List<Employee> getEmployees();
    void updateEmployee(Employee employee);
    Optional<Employee> getEmployeeByID(int id);
    List<Employee> getEmployeeByName(String name);
    List<Employee> getEmployeeBySurname(String surname);
    boolean deleteEmployee(Employee employee);
}
