package app.service;

import app.DTO;
import app.entity.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class DTOServiceByList implements DTO {

    private List<Employee> employees = new ArrayList<>();

    @Override
    public boolean addEmployee(Employee employee) {
        return employees.add(employee);
    }

    @Override
    public List<Employee> getEmployees() {
        return employees;
    }

    @Override
    public void updateEmployee(Employee employee) {

        boolean hasEmployee = employees.stream()
                .anyMatch(e -> e.getId() == employee.getId());

        if (hasEmployee) {
            employees.stream()
                    .filter(e -> e.getId() == employee.getId())
                    .forEach(e -> {
                        e.setId(employee.getId());
                        e.setName(employee.getName());
                        e.setSurname(employee.getSurname());
                        e.setJob(employee.getJob());
                        e.setSalary(employee.getSalary());
                    });
        }
        else {
            employees.add(Employee.of(employee.getName(), employee.getSurname(), employee.getJob(), employee.getSalary()));
        }
    }

    @Override
    public Optional<Employee> getEmployeeByID(int id) {
       return employees.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst();
    }

    @Override
    public List<Employee> getEmployeeByName(String name) {
        return employees.stream()
                .filter(e -> e.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public List<Employee> getEmployeeBySurname(String surname) {
        return employees.stream()
                .filter(e -> e.getName().equals(surname))
                .collect(Collectors.toList());
    }

    @Override
    public boolean deleteEmployee(Employee employee) {
        Optional<Employee> first = employees.stream()
                .filter(e -> e.getId() == employee.getId())
                .findFirst();

        if (first.isPresent()) {
            return employees.remove(employee);
        }
        return false;
    }
}
