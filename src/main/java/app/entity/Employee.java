package app.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Employee {

    private static long ID = 0;
    private long id;
    private String name;
    private String surname;
    private String job;
    private BigDecimal salary;

    private Employee(String name, String surname, String job, BigDecimal salary) {
        this.id = ++ID;
        this.name = name;
        this.surname = surname;
        this.job = job;
        this.salary = salary;
    }

    public static Employee of(String name, String surname, String job, BigDecimal salary) {
        return new Employee(name, surname, job, salary);
    }

    public String toString() {
        return String.format("ID = %d | Name = %s | Surname = %s | Job = %s | Salary = %s\n",id, name, surname, job, salary);
    }
}
